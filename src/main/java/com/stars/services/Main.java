package main.java.com.stars.services;

public class Main {
    public static void main(String[] args) {
        StarFiguresDrawingService starFiguresDrawingService = new StarFiguresDrawingService();

        starFiguresDrawingService.drawFilledSquare();
        System.out.println();
        starFiguresDrawingService.drawEmptySquare();
        System.out.println();
        starFiguresDrawingService.drawEmptyRightTriangleUpL();
        System.out.println();
        starFiguresDrawingService.drawEmptyRightTriangleDownL();
        System.out.println();
        starFiguresDrawingService.drawEmptyRightTriangleDownR();
        System.out.println();
        starFiguresDrawingService.drawEmptyRightTriangleUpR();
        System.out.println();
        starFiguresDrawingService.drawX();
        System.out.println();
        starFiguresDrawingService.drawTriangleOnTopLine();
        System.out.println();
        starFiguresDrawingService.drawTriangleOnBottomLine();
        System.out.println();
    }
}
