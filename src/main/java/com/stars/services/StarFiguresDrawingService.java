package main.java.com.stars.services;

public class StarFiguresDrawingService {
    private static final char STAR = (Character) ((char)0x2605);
    private static final char SPACE = ' ';

    static final int FIXED_WIDTH = 80;
    private char[][] figure;

    public void drawFilledSquare(){
        figure = new char[7][7];
        for (int i = 0; i < figure.length; i++){
            for (int j = 0; j < figure[i].length; j++) {
                figure[i][j] = STAR;
            }
        }
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawEmptySquare(){
        figure = new char[7][7];
        for (int i = 0; i < figure.length; i++){
            for (int j = 0; j < figure[i].length; j++) {
                if (i == 0 || i == figure.length - 1){
                    figure[i][j] = STAR;
                }
                else if (j == 0 || j == figure[i].length - 1){
                    figure[i][j] = STAR;
                }
                else {
                    figure[i][j] = SPACE;
                }
            }
        }
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawEmptyRightTriangleUpL(){
        figure = new char[7][7];
        int counter = 5;
        for (int i = 0; i < figure.length; i++){
            for (int j = 0; j < figure[i].length; j++) {
                if (i == 0){
                    figure[i][j] = STAR;
                }
                else if (j == 0){
                    figure[i][j] = STAR;

                }
                else if (j == counter){
                    figure[i][j] = STAR;
                    counter--;
                }
                else {
                    figure[i][j] = SPACE;
                }
            }
        }
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawEmptyRightTriangleDownL(){
        figure = new char[7][7];
        for (int i = 0; i < figure.length; i++){
            for (int j = 0; j < figure[i].length; j++) {
                if (i == figure.length -1){
                    figure[i][j] = STAR;
                }
                else if (j == 0){
                    figure[i][j] = STAR;
                }
                else if (i == j){
                    figure[i][j] = STAR;
                }
                else {
                    figure[i][j] = SPACE;
                }
            }
        }
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawEmptyRightTriangleDownR(){
        figure = new char[7][7];
        int counter = 6;
        for (int i = 0; i < figure.length; i++){
            for (int j = 0; j < figure[i].length; j++) {
                if (i == figure.length - 1){
                    figure[i][j] = STAR;
                }
                else if (j == counter){
                    figure[i][j] = STAR;
                    counter--;
                }
                else if (j == figure[i].length - 1){
                    figure[i][j] = STAR;
                }

                else {
                    figure[i][j] = SPACE;
                }
            }
        }
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawEmptyRightTriangleUpR(){
        figure = new char[7][7];
        for (int i = 0; i < figure.length; i++){
            for (int j = 0; j < figure[i].length; j++){
                if (i == 0){
                    figure[i][j] = STAR;
                }
                else if (j == figure[i].length - 1){
                    figure[i][j] = STAR;
                }
                else if (i == j){
                    figure[i][j] = STAR;
                }
                else {
                    figure[i][j] = SPACE;
                }
            }
        }
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawX(){
        figure = new char[7][7];
        for (int i = 0; i < figure.length; i++){
            for (int j = 0; j < figure[i].length; j++){
                if (i == j || (i + j) == 6){
                    figure[i][j] = STAR;
                }
                else {
                    figure[i][j] = SPACE;
                }
            }
        }
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawTriangleOnTopLine(){
        figure = new char[7][7];
        for (int i = 0; i < figure.length; i++){
            for (int j = 0; j < figure[i].length; j++){
                if (i == 0){
                    figure[i][j] = STAR;
                }
                else if (i == j || (i + j) == 6){
                    if (i < 4){
                        figure[i][j] = STAR;
                    }
                    else {
                        figure[i][j] = SPACE;
                    }
                }
                else {
                    figure[i][j] = SPACE;
                }
            }
        }
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }

    public void drawTriangleOnBottomLine(){
        figure = new char[7][7];
        for (int i = 0; i < figure.length; i++){
            for (int j = 0; j < figure[i].length; j++){
                if (i == figure.length - 1){
                    figure[i][j] = STAR;
                }
                else if (i == j || (i + j) == 6){
                    if (i > 2){
                        figure[i][j] = STAR;
                    }
                    else {
                        figure[i][j] = SPACE;
                    }
                }
                else {
                    figure[i][j] = SPACE;
                }
            }
        }
        for (int i = 0; i < figure.length; i++){
            for (char drawingSymbol : figure[i]){
                System.out.print(drawingSymbol + "\t");
            }
            System.out.println();
        }
    }
}
